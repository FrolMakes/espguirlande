#include <Arduino.h>
#include <FastLED.h>

#define LED_PIN 4
#define NUM_LEDS 50
#define MAX_BRIGHTNESS 255
#define LED_TYPE WS2811
#define COLOR_ORDER GRB
#define UPDATES_PER_SECOND 100

#define NUMBER_OF_COLORS 5

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

CRGB BLACK;

CRGB leds[NUM_LEDS];

typedef enum {
    OFF,
    STATIC,
    DYNAMIC
} MODE;

CRGB colors[] = {
        CHSV(HUE_AQUA, 255, 255),
        CHSV(HUE_RED, 255, 255),
        CHSV(HUE_GREEN, 255, 255),
        CHSV(HUE_PURPLE, 255, 255),
        CHSV(HUE_ORANGE, 255, 255),

};


int colorIndex = 0;
CRGB currentColor = colors[1];

MODE currentMode = DYNAMIC;

int paletteIndex = 0;
int dynamicStartIndex = 0;

CRGBPalette16 palettes[] = {
        RainbowColors_p,
        RainbowStripeColors_p,
        RainbowStripeColors_p,
        CloudColors_p,
        PartyColors_p};

TBlendType blendings[] = {
        LINEARBLEND,
        NOBLEND,
        LINEARBLEND,
        LINEARBLEND,
        LINEARBLEND};

int currentBrightness = MAX_BRIGHTNESS;
int brightnessDiff = -1;

void setup() {
    Serial.begin(9600);
    Serial.println("Starting Setup...");

    delay(3000); // power-up safety delay
    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
    FastLED.setBrightness(currentBrightness);

}

int nblendU8TowardU8(int cur, const int target, int amount) {
    if (cur == target)
        return 0;

    if (cur < target) {

        int delta = target - cur;
        delta = scale8_video(delta, amount);
        return cur + delta;
    } else {
        int delta = cur - target;
        delta = scale8_video(delta, amount);
        return cur - delta;
    }
}

CRGB fadeTowardColor(CRGB cur, const CRGB target, int amount) {
    return CRGB(
            nblendU8TowardU8(cur.red, target.red, amount),
            nblendU8TowardU8(cur.green, target.green, amount),
            nblendU8TowardU8(cur.blue, target.blue, amount));
}

void fillLEDsFromPaletteColors(int colorIndex) {
    int brightness = 255;

    for (int i = 0; i < NUM_LEDS; i++) {
        leds[i] = ColorFromPalette(palettes[paletteIndex], colorIndex, brightness, blendings[paletteIndex]);
        colorIndex += 3;
    }
}

void loop() {


    FastLED.setBrightness(currentBrightness);

    if (currentMode == STATIC || currentMode == OFF) {
        CRGB targetColor = (currentMode == STATIC) ? colors[colorIndex] : CRGB(0, 0, 0);
        if (currentColor != targetColor) {
            currentColor = fadeTowardColor(currentColor, targetColor, 10);
        }
        fill_solid(leds, NUM_LEDS, currentColor);
    } else if (currentMode == DYNAMIC) {
        fillLEDsFromPaletteColors(dynamicStartIndex++);
    }

    static int startIndex = 0;

    Serial.print("Colors ");
    Serial.print(leds[0].r);
    Serial.print(" - ");
    Serial.print(leds[0].g);
    Serial.print(" - ");
    Serial.print(leds[0].b);
    Serial.println("");

    FastLED.show();
    FastLED.delay(1000 / UPDATES_PER_SECOND);
}
